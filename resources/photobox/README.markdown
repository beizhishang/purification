# Photobox

A Pen created on CodePen.io. Original URL: [https://codepen.io/vsync/pen/upeBw](https://codepen.io/vsync/pen/upeBw).

Photobox is the evolution, the next generation of gallery UI & UX code. It can do anything. It's super flexible.
